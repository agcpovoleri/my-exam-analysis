import { ValidationPipe } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { AppModule } from './app.module';
import { GlobalErrorsInterceptor } from './common/interceptors/global-errors.interceptor';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.useGlobalPipes(new ValidationPipe());
  app.useGlobalInterceptors(new GlobalErrorsInterceptor());
  startSwagger(app);
  await app.listen(3000);
}

function startSwagger(app) {
  const options = new DocumentBuilder()
    .setTitle('MyExam Result analysis')
    .setDescription('Taking care of your exam info')
    .setVersion('1.0')
    .build();
  const document = SwaggerModule.createDocument(app, options);
  SwaggerModule.setup('api', app, document);
}
bootstrap();
