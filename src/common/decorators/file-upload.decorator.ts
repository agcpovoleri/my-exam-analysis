import { ApiBody } from '@nestjs/swagger';

export const ApiFileUpload = (fileName = 'file', description = '', required = true): MethodDecorator => (
    target: any,
    propertyKey: string,
    descriptor: PropertyDescriptor,
) => {
    ApiBody({
        required: required,
        description: description,
        schema: {
            type: 'object',
            properties: {
                [fileName]: {
                    type: 'string',
                    format: 'binary',
                },
            },
        },
    })(target, propertyKey, descriptor);
};