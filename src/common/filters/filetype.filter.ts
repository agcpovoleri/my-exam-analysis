import { InvalidUploadedFileFormatException } from "../../../src/exams/exceptions/invalid-fileformat.exception";

export const ImageFileFilter = (req: any, file: any, callback: any) => {
    if (!file.originalname.match(/.*(\.csv)$/)) {
        callback(new InvalidUploadedFileFormatException(file.mimetype), false);
    } 
    callback(null, true);
};
  