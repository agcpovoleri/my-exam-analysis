import { Module } from '@nestjs/common';
import { APP_INTERCEPTOR } from '@nestjs/core';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Connection } from 'typeorm';
import { GlobalErrorsInterceptor } from './common/interceptors/global-errors.interceptor';
import { ExamsModule } from './exams/exams.module';

@Module({
  imports: [TypeOrmModule.forRoot(), ExamsModule],
  controllers: [],
  providers: [{
    provide: APP_INTERCEPTOR,
    useClass: GlobalErrorsInterceptor,
  }],
})
export class AppModule {
  constructor(private connection: Connection) {}
}
