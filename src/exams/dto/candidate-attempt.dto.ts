import { IsEnum, IsNotEmpty } from 'class-validator';
import { AnswerOptionEnum } from '../../../src/exams/enums/answer-options.enum';

export class CandidateAttemptDto {
  @IsNotEmpty()
  email: string;

  name: string;

  @IsNotEmpty()
  answers: CandidateAnsweredQuestionDto[];
}

export class CandidateAnsweredQuestionDto {
  @IsNotEmpty()
  questionNumber: number;

  @IsNotEmpty()
  @IsEnum(AnswerOptionEnum)
  answer: AnswerOptionEnum;

  constructor(questionNumber: number, answer:AnswerOptionEnum) {
    this.questionNumber = questionNumber;
    this.answer =answer;
  }
}
