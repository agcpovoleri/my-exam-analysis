export class CsvHeaderExpected {
  static EXAM_ID = 'Exam Id';
  static EXAM_DATE = 'Exam Date';
  static CANDIDATE_EMAIL = 'Candidate Email';
  static CANDIDATE_NAME = 'Candidate Name';
  static QUESTION_NO = 'Question Number';
  static ANSWER = 'Answer';
}
