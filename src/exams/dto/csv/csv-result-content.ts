import { CandidateAttemptDto } from "../candidate-attempt.dto";

export class CsvExportContent {
  
  static EXAM_ID = { label: 'Exam Id', field: 'examId' };
  static AVERAGE_SCORE = { label: 'Average Score', field: 'avgScore' };
  static CANDIDATE_EMAIL = {
    label: 'Candidate Email',
    field: 'candidateEmail',
  };
  static CANDIDATE_NAME = { label: 'Candidate Name', field: 'candidateName' };
  static SCORE = { label: 'Score', field: 'score' };
  static PERCENT_RANK = { label: 'Percent Rank', field: 'percentrank' };

  examId: string;
  avgScore: string;
  candidateEmail: string;
  candidateName: string;
  score: string;
  percentrank: string;

  constructor(examId: string, avgScore:string, examAnswersFromCandidate: CandidateAttemptDto, score: string, percentrank: string) {
    this.examId = examId;
    this.avgScore = avgScore;
    this.candidateEmail = examAnswersFromCandidate.email;
    this.candidateName = examAnswersFromCandidate.name;
    this.score = score;
    this.percentrank = percentrank;
  };

}

export const CsvExportFields = [
  {
    label: CsvExportContent.EXAM_ID.label,
    value: CsvExportContent.EXAM_ID.field,
  },
  {
    label: CsvExportContent.AVERAGE_SCORE.label,
    value: CsvExportContent.AVERAGE_SCORE.field,
  },
  {
    label: CsvExportContent.CANDIDATE_EMAIL.label,
    value: CsvExportContent.CANDIDATE_EMAIL.field,
  },
  {
    label: CsvExportContent.CANDIDATE_NAME.label,
    value: CsvExportContent.CANDIDATE_NAME.field,
  },
  {
    label: CsvExportContent.SCORE.label,
    value: CsvExportContent.SCORE.field,
  },
  {
    label: CsvExportContent.PERCENT_RANK.label,
    value: CsvExportContent.PERCENT_RANK.field,
  },
];
