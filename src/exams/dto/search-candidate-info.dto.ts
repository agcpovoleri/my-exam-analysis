import { ApiProperty } from '@nestjs/swagger';

export class SearchCandidateExamInfoDto {
  
  @ApiProperty()
  examId: string;

  @ApiProperty()
  candidateEmail: string;

}