import { IsArray, IsEnum, IsNotEmpty } from 'class-validator';
import { AnswerOptionEnum } from '../enums/answer-options.enum';

export class QuestionDto {
  @IsNotEmpty()
  question: number;

  @IsNotEmpty()
  @IsEnum(AnswerOptionEnum)
  rightAnswer: AnswerOptionEnum;
}

export class ExamInformationDto {
  
  @IsNotEmpty()
  name: string;

  @IsNotEmpty()
  @IsArray()
  questions: QuestionDto[];
}
