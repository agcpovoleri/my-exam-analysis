import { IsEnum, IsNotEmpty, Max, Min} from 'class-validator';
import { AnswerOptionEnum } from '../../../src/exams/enums/answer-options.enum';
import { CsvHeaderExpected } from './csv/csv-import-content';
export class CreateUserAnswerDto {
  
  constructor(csvContent: []) {
    this.examName = csvContent[CsvHeaderExpected.EXAM_ID];
    this.examDate = csvContent[CsvHeaderExpected.EXAM_DATE];
    this.questionNumber = csvContent[CsvHeaderExpected.QUESTION_NO];
    this.answer = csvContent[CsvHeaderExpected.ANSWER];
    this.candidateName = csvContent[CsvHeaderExpected.CANDIDATE_NAME];
    this.candidateEmail =  csvContent[CsvHeaderExpected.CANDIDATE_EMAIL];
    this.questionId = null;
  };

  @IsNotEmpty()
  examName: number;
  
  @IsNotEmpty()
  examDate: string;

  @Min(1)
  @Max(999)
  @IsNotEmpty()
  questionNumber: number;

  @IsNotEmpty()
  questionId: number;

  @IsEnum(AnswerOptionEnum)
  @IsNotEmpty()
  answer: string;

  @IsNotEmpty()
  candidateName: string;

  @IsNotEmpty()
  candidateEmail: string;

}
