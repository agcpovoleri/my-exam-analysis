import { AnswerOptionEnum } from '../../../src/exams/enums/answer-options.enum';
import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  ManyToOne,
  JoinColumn,
} from 'typeorm';
import { CandidateAnsweredQuestionDto, CandidateAttemptDto } from '../dto/candidate-attempt.dto';
import { CreateUserAnswerDto } from '../dto/candidate-answer.dto';
import { Question } from './question.entity';
import * as _ from 'lodash';

@Entity()
export class UserAnswer {
  
  @PrimaryGeneratedColumn()
  public id: number;

  @Column({ type: 'date' })
  public exam_date: Date;

  @Column()
  public question_id: number;

  @Column({
    type: 'enum',
    enum: AnswerOptionEnum,
    default: null,
  })
  public answer: AnswerOptionEnum;

  @Column()
  public candidate_name: string;

  @Column()
  public candidate_email: string;

  @ManyToOne(() => Question)
  @JoinColumn({name: 'question_id', referencedColumnName: 'id'})
  question: Question;

  static transformToEntity(source: CreateUserAnswerDto): UserAnswer {
    
    const userAnswer = new UserAnswer();
    userAnswer.exam_date = new Date(source.examDate);
    userAnswer.question_id = source.questionId;
    userAnswer.candidate_name = source.candidateName;
    userAnswer.candidate_email = source.candidateEmail;
    userAnswer.answer = AnswerOptionEnum[source.answer];

    return userAnswer;
  }

  static transformToCandidateAttempt(examsWithResults: UserAnswer[]): CandidateAttemptDto[] {

    //Not good practice to use the context here but it was needed due to the class scope
    const context = this;
    const candidateAttempts: CandidateAttemptDto[] = [];
    const grouped = _.groupBy(examsWithResults, function (examsWithResult) {
      return examsWithResult.candidate_email;
    });
    _.forOwn(grouped, function (value, key) {
      const candidateAttempt: CandidateAttemptDto = {
        name: "",
        email: key,
        answers: context.transformAnswers(value).answers,
      };
      candidateAttempts.push(candidateAttempt);
    });
    return candidateAttempts;
  }

  static transformAnswers(examsWithResults: UserAnswer[]): CandidateAttemptDto {
    const candidateAttempt: CandidateAttemptDto = new CandidateAttemptDto();
    
    const examAnsweredQuestions: CandidateAnsweredQuestionDto[] = [];
    if (examsWithResults !== undefined) {
      examsWithResults.forEach((answeredInfo) => {

        candidateAttempt.name = answeredInfo.candidate_name;
        candidateAttempt.email = answeredInfo.candidate_email;

        const userAnswer = new CandidateAnsweredQuestionDto(answeredInfo.question.question, answeredInfo.answer);
        examAnsweredQuestions.push(userAnswer);
      });
    }
    candidateAttempt.answers = examAnsweredQuestions;
    return candidateAttempt;
  }
}
