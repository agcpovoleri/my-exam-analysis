import { Entity, Column, PrimaryGeneratedColumn, OneToMany } from 'typeorm';
import { ExamInformationDto, QuestionDto } from '../dto/exam-template.dto';
import { Question } from './question.entity';

@Entity()
export class Exam {
  
  @PrimaryGeneratedColumn()
  public id: number;

  @Column({
    type: 'varchar',
    length: 8,
    unique: true,
    nullable: false,
  })
  public name: string;

  @OneToMany((type) => Question, (question) => question.exam)
  questions: Question[];

  
  static convertEntityToDto(examInformation: Exam): ExamInformationDto {
    let examTemplate: ExamInformationDto;
    
    if (examInformation !== undefined) {
      examTemplate = new ExamInformationDto();
      examTemplate.name = examInformation.name;

      const questions: QuestionDto[] = [];
      examInformation.questions.forEach((questionInformation) => {
        questions.push({
          question: questionInformation.question,
          rightAnswer: questionInformation.answer,
        });
      });
      examTemplate.questions = questions;
    }
    return examTemplate;
  }
}
