import {
  Column, Entity,
  JoinColumn, ManyToOne,
  OneToMany, PrimaryGeneratedColumn
} from 'typeorm';
import { AnswerOptionEnum } from '../enums/answer-options.enum';
import { Exam } from './exam.entity';
import { UserAnswer } from './user-answer.entity';

@Entity()
export class Question {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  exam_id: number;

  @Column()
  question: number;

  @Column({
    type: 'enum',
    enum: AnswerOptionEnum,
    default: null,
  })
  answer: AnswerOptionEnum;

  @ManyToOne(() => Exam)
  @JoinColumn({ name: 'exam_id', referencedColumnName: 'id' })
  exam: Exam;

  @OneToMany(() => UserAnswer, (userAnswer) => userAnswer.question)
  userAnswers: UserAnswer[];
}
