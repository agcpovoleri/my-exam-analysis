import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CsvModule } from 'nest-csv-parser';
import { ExamsController } from './controllers/exams.controller';
import { Exam } from './entities/exam.entity';
import { Question } from './entities/question.entity';
import { UserAnswer } from './entities/user-answer.entity';
import { CsvHelper } from './helpers/csv-helper';
import { CandidateAnswerRepository } from './repositories/candidate-answer.repository';
import { ExamRepository } from './repositories/exam.repository';
import { ExamEvaluationService } from './services/exam-evaluation.service';
import { ExamsService } from './services/exams.service';
import { UserAnswerService } from './services/user-answers.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([Exam, Question, UserAnswer, ExamRepository, CandidateAnswerRepository]),
    CsvModule,
  ],
  exports: [CsvHelper],
  providers: [ExamsService, UserAnswerService, ExamEvaluationService, CsvHelper],
  controllers: [ExamsController],
})
export class ExamsModule { }
