import { Injectable } from '@nestjs/common';
import { EntityRepository, Repository } from 'typeorm';
import { ExamInformationDto } from '../dto/exam-template.dto';
import { Exam } from '../entities/exam.entity';

@Injectable()
@EntityRepository(Exam)
export class ExamRepository extends Repository<Exam> {
  
  async checkIfExamExists(name: string): Promise<boolean> {
    const count = await this.count({ where: { name: name } });
    return count > 0;
  }

  async getAllExams(): Promise<Exam[]> {
    const examsWithQuestions = await this.createQueryBuilder('exam')
      .leftJoinAndSelect('exam.questions', 'question')
      .orderBy('exam.id', 'ASC')
      .addOrderBy('question.id', 'ASC')
      .getMany();
    return examsWithQuestions;
  }
  
  async findExamTemplateWithCorrectAnswers(examId: any): Promise<ExamInformationDto> {
    const questions = await this.createQueryBuilder('exam')
      .select('exam')
      .addSelect('question')
      .innerJoin('exam.questions', 'question')
      .where('exam.name = :name', { name: examId })
      .orderBy('exam.id', 'ASC')
      .addOrderBy('question.id', 'ASC')
      .getOne();

    return Exam.convertEntityToDto(questions);
  }
}
