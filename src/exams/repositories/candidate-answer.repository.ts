import { Injectable } from '@nestjs/common';
import { EntityRepository, Repository } from 'typeorm';
import {
  CandidateAttemptDto
} from '../dto/candidate-attempt.dto';
import { CreateUserAnswerDto } from '../dto/candidate-answer.dto';
import { UserAnswer } from '../entities/user-answer.entity';


@Injectable()
@EntityRepository(UserAnswer)
export class CandidateAnswerRepository extends Repository<UserAnswer> {
  
  upsert(createUserAnswerDto: CreateUserAnswerDto) {
    this.save(UserAnswer.transformToEntity(createUserAnswerDto));
  }

  async findAllCandidateAttemptsByExam(examId: any): Promise<CandidateAttemptDto[]> {
    const examsWithResults = await this.createQueryBuilder('userAnswer')
      .select('userAnswer')
      .addSelect('question')
      .addSelect('exam')
      .leftJoin('userAnswer.question', 'question')
      .leftJoin('question.exam', 'exam')
      .where('exam.name = :examId', {
        examId: examId,
      })
      .addOrderBy('question.id', 'ASC')
      .getMany();

    return UserAnswer.transformToCandidateAttempt(examsWithResults);
  }

  async findCandidateAttemptByExam(candidadeEmail: any, examId: any): Promise<CandidateAttemptDto> {
    const examsWithResults = await this.createQueryBuilder('userAnswer')
      .select('userAnswer')
      .addSelect('question')
      .addSelect('exam')
      .leftJoin('userAnswer.question', 'question')
      .leftJoin('question.exam', 'exam')
      .where('userAnswer.candidate_email = :email AND exam.name = :examId', {
        email: candidadeEmail,
        examId: examId,
      })
      .addOrderBy('question.id', 'ASC')
      .getMany();

    return UserAnswer.transformAnswers(examsWithResults);
  }
}
