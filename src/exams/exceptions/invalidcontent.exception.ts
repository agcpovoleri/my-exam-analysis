import { BadRequestException } from '@nestjs/common';

export class InvalidCSVContentException extends BadRequestException {
  constructor() {
    super('Unable to process the Invalid content');
  }
}
