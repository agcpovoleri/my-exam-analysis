import { UnsupportedMediaTypeException } from '@nestjs/common';

export class InvalidUploadedFileFormatException extends UnsupportedMediaTypeException {
  constructor(mimetype: string) {
    super('Unsupported file type: ' + mimetype);
  }
}
