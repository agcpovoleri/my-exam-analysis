import { BadRequestException } from "@nestjs/common";

export class BusinessException extends BadRequestException {
    constructor(errorMessage: string) {
        super(errorMessage);
    }
}