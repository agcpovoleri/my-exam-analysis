import { Test } from '@nestjs/testing';
import { ExamEvaluationService } from './exam-evaluation.service';
import { TestContentHelper } from '../../../test/test-content-helper';

describe('ExamEvaluationService', () => {

    let examEvaluationService: ExamEvaluationService;
    beforeEach(async (done) => {
        const moduleRef = await Test.createTestingModule({
            controllers: [],
            providers: [ExamEvaluationService],
            imports: []
        }).compile();

        examEvaluationService = moduleRef.get<ExamEvaluationService>(ExamEvaluationService);
        done();
    });

    describe('Calculate percentage score of Exam candidate attemp', () => {
        test('Should calculate Zero score based only on Wrong candidate answers', () => {
            const examTemplate = TestContentHelper.getExamTemplateSample();
            const candidateAttempt = TestContentHelper.getCandidateAttemptOnlyWithInvalidAnswers();
            
            const calculatedScore = examEvaluationService.calculateScore(examTemplate, candidateAttempt.answers);
            expect(calculatedScore).toBe(0);
        });

        test('Should calculate Maximum score based only on Right candidate answers', () => {
            const examTemplate = TestContentHelper.getExamTemplateSample();
            const candidateAttempt = TestContentHelper.getCandidateAttemptOnlyWithValidAnswers();
            
            const calculatedScore = examEvaluationService.calculateScore(examTemplate, candidateAttempt.answers);
            expect(calculatedScore).toBe(100);
        });

        test('Should calculate percentage score based on incomplete/invalid candidate answers', () => {
            const examTemplate = TestContentHelper.getExamTemplateSample();
            const candidateAttempt = TestContentHelper.getCandidateAttemptWithMissingAnswers();
            
            const calculatedScore = examEvaluationService.calculateScore(examTemplate, candidateAttempt.answers);
            expect(calculatedScore).toBeGreaterThanOrEqual(0);
            expect(calculatedScore).toBeLessThan(100);
        });
    });

    describe('Retrieve list of scores based on all candidate attempts', () => {
        test('Should list of scores be empty if there is no candidate attempts to exam', () => {
   
            const examTemplate = TestContentHelper.getExamTemplateSample();
            const candidateAttempts = [];
           
            const scores = examEvaluationService.scoresInTheExam(examTemplate, candidateAttempts);
            expect(scores).toBeInstanceOf(Array);
            expect(scores.length).toBe(0);
        });

        test('Should list of scores have the same number of items as the candidate attempts', () => {
            const examTemplate = TestContentHelper.getExamTemplateSample();
            const candidateAttempts = [];
            const candidateAttempt = TestContentHelper.getCandidateAttemptWithMissingAnswers();
            candidateAttempts.push(candidateAttempt);

            const scores = examEvaluationService.scoresInTheExam(examTemplate, candidateAttempts);
            
            expect(scores).toBeInstanceOf(Array);
            expect(scores.length).toBe(candidateAttempts.length);
        });
    });
});


