import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import * as _ from 'lodash';
import { CandidateAttemptDto } from '../dto/candidate-attempt.dto';
import { CreateUserAnswerDto } from '../dto/candidate-answer.dto';
import { CandidateAnswerRepository } from '../repositories/candidate-answer.repository';
import { BusinessException } from '../exceptions/business.exception';


@Injectable()
export class UserAnswerService {
  constructor(@InjectRepository(CandidateAnswerRepository) private candidateAnswerRepository) {}

  create(createUserAnswerDto: CreateUserAnswerDto) {
    this.candidateAnswerRepository.upsert(createUserAnswerDto);
  }

  async validateCandidateAttempt(candidateEmail: string, examName: string) {
    await this.findCandidateAnswersAtExam(candidateEmail, examName)
    .then(candidateAttempt => {
      if (!(Array.isArray(candidateAttempt.answers) && candidateAttempt.answers.length > 0)) 
        throw new BusinessException('Candidate has not answered the given Exam: ' + examName);
    });
  }
  
  async findCandidateAnswersAtExam(candidadeEmail: any, examId: any): Promise<CandidateAttemptDto> {
    const examsWithResults = await this.candidateAnswerRepository.findCandidateAttemptByExam(candidadeEmail, examId);
    return await examsWithResults;
  }

  async findAllCandidateAttemptToExam(examName: string): Promise<CandidateAttemptDto[]> {
    const candidateAnswers: CandidateAttemptDto[] = await this.candidateAnswerRepository.findAllCandidateAttemptsByExam(examName);
    return await candidateAnswers;
  }
}
