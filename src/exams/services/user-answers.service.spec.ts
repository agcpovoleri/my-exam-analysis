import { Test } from '@nestjs/testing';
import { CsvModule } from 'nest-csv-parser';
import { CandidateAnsweredQuestionDto, CandidateAttemptDto } from '../dto/candidate-attempt.dto';
import { AnswerOptionEnum } from '../enums/answer-options.enum';
import { BusinessException } from '../exceptions/business.exception';
import { CandidateAnswerRepository } from '../repositories/candidate-answer.repository';
import { UserAnswerService } from './user-answers.service';

describe('UserAnswerService', () => {

    let userAnswerService: UserAnswerService;
    let candidateAnswerRepository: CandidateAnswerRepository;

    beforeEach(async (done) => {
        const moduleRef = await Test.createTestingModule({
            controllers: [],
            providers: [UserAnswerService, CandidateAnswerRepository],
            imports: [CsvModule]
        }).compile();

        candidateAnswerRepository = moduleRef.get<CandidateAnswerRepository>(CandidateAnswerRepository);
        userAnswerService = moduleRef.get<UserAnswerService>(UserAnswerService);
        done();

       
    });

    describe('Search by Candidate Attempts', () => {
        
        test('Should do nothing if candidate answers contains answers', async (done) => {
            const expectedResult = new CandidateAttemptDto();
            const answers = new CandidateAnsweredQuestionDto(1, AnswerOptionEnum.A)
            expectedResult.answers = [answers];

            jest.spyOn(userAnswerService, 'findCandidateAnswersAtExam').mockImplementation(() => Promise.resolve(expectedResult));;
            userAnswerService.validateCandidateAttempt("aby","any").then(() => {
                done();
            });
            
        });
        
        test('Should throw BusinessException if candidate attempt not found', async (done) => {
            const expectedResult: CandidateAttemptDto = new CandidateAttemptDto();
            expectedResult.answers = [];
            jest.spyOn(userAnswerService, 'findCandidateAnswersAtExam').mockImplementation(() => Promise.resolve(expectedResult));;

            try {
                userAnswerService.validateCandidateAttempt("aby","any").catch(e => {
                    expect(e).toBeInstanceOf(BusinessException);
                    done();
                });
            } catch(e) {
                done(e);
            }
        });

        test('Should call repository to search by candidate attempt', async (done) => {
            const expectedResult: CandidateAttemptDto = new CandidateAttemptDto();
            expectedResult.answers = [];
            const repositoryCall = jest.spyOn(candidateAnswerRepository, 'findCandidateAttemptByExam').mockImplementation(() => Promise.resolve(expectedResult));;
            try {
                userAnswerService.findCandidateAnswersAtExam("aby","aby").then(data => {
                    expect(repositoryCall).toHaveBeenCalled();
                    done();
                });
            } catch(e) {
                done(e);
            }
        });

        test('Should call repository to search for ALL candidate attempts', async (done) => {
            const repositoryCall = jest.spyOn(candidateAnswerRepository, 'findAllCandidateAttemptsByExam').mockImplementation(() => Promise.resolve([]))
            try {
                userAnswerService.findAllCandidateAttemptToExam("aby").then(() => {
                    expect(repositoryCall).toHaveBeenCalled();
                    done();
                });
            } catch(e) {
                done(e);
            }
        });
    });
});