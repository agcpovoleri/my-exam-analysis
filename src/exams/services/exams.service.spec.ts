import { Test } from '@nestjs/testing';
import { CsvModule } from 'nest-csv-parser';
import { CreateUserAnswerDto } from '../dto/candidate-answer.dto';
import { CandidateAnsweredQuestionDto, CandidateAttemptDto } from '../dto/candidate-attempt.dto';
import { ExamInformationDto, QuestionDto } from '../dto/exam-template.dto';
import { SearchCandidateExamInfoDto } from '../dto/search-candidate-info.dto';
import { AnswerOptionEnum } from '../enums/answer-options.enum';
import { BusinessException } from '../exceptions/business.exception';
import { InvalidCSVContentException } from '../exceptions/invalidcontent.exception';
import { CsvHelper } from '../helpers/csv-helper';
import { ExamResultEvaluationHelper } from '../helpers/exam-result-evaluation-helper';
import { CandidateAnswerRepository } from '../repositories/candidate-answer.repository';
import { ExamRepository } from '../repositories/exam.repository';
import { ExamEvaluationService } from './exam-evaluation.service';
import { ExamsService } from './exams.service';
import { UserAnswerService } from './user-answers.service';

describe('ExamsService', () => {

    let userAnswerService: UserAnswerService;
    let examRepository: ExamRepository;
    let examService: ExamsService;
    let csvHelper: CsvHelper;

    const search = new SearchCandidateExamInfoDto();
    search.examId = "any";
    search.candidateEmail = "any"

    beforeEach(async (done) => {
        const moduleRef = await Test.createTestingModule({
            controllers: [],
            providers: [ExamsService, ExamEvaluationService, UserAnswerService, ExamRepository, CandidateAnswerRepository, CsvHelper, ExamResultEvaluationHelper],
            imports: [CsvModule]
        }).compile();

        examRepository = moduleRef.get<ExamRepository>(ExamRepository);
        userAnswerService = moduleRef.get<UserAnswerService>(UserAnswerService);
        examService = moduleRef.get<ExamsService>(ExamsService);
        csvHelper = moduleRef.get<CsvHelper>(CsvHelper);
        done();
    });

    describe('Search by Candidate Attempts', () => {
        
        test('Should throw BusinessException if exam is not found', async (done) => {
            jest.spyOn(examRepository, 'checkIfExamExists').mockImplementation(() => Promise.resolve(false));
            await examService.validateCandidateAttemptToExam(search).catch(err => {
                expect(err).toBeInstanceOf(BusinessException);
                done();
            });
        });

        test('Should throw BusinessException if candidate attempt is not found', async (done) => {
            
            const candidateAttempt = new CandidateAttemptDto();
            candidateAttempt.answers = [];

            const checkExamExistence = jest.spyOn(examRepository, 'checkIfExamExists').mockImplementation(() => Promise.resolve(true));
            jest.spyOn(userAnswerService, 'findCandidateAnswersAtExam').mockImplementation(() => Promise.resolve(candidateAttempt));
            await examService.validateCandidateAttemptToExam(search).catch(err => {
                expect(checkExamExistence).toHaveBeenCalled();
                expect(err).toBeInstanceOf(BusinessException);
                done();
            });
        });

        test('Should validation pass if exam and candidate attempt exists', async (done) => {
            
            const candidateAttempt = getCandidateAttemptSample(search);
            
            const checkExamExistence = jest.spyOn(examRepository, 'checkIfExamExists').mockImplementation(() => Promise.resolve(true));
            const checkCandidateAttemptExistence = jest.spyOn(userAnswerService, 'findCandidateAnswersAtExam').mockImplementation(() => Promise.resolve(candidateAttempt));
            await examService.validateCandidateAttemptToExam(search).then(() => {
                expect(checkExamExistence).toHaveBeenCalled();
                expect(checkCandidateAttemptExistence).toHaveBeenCalled();
                done();
            });
        });
    });

    describe('Persist Candidate Answers', () => {
        
        test('Should throw Exception when invalid csv content is given', async (done) => {
            jest.spyOn(csvHelper, 'validateCsvFile').mockImplementation(() => {
                throw new InvalidCSVContentException();
            });
            const file = 'any';
            await examService.persistCandidateAnswers(file).catch(err => {
                expect(err).toBeInstanceOf(InvalidCSVContentException);
                done();
            });
        });

        test('Should parse CSV content if valid CSV file is given', async (done) => {
            const validateCsvCall = jest.spyOn(csvHelper, 'validateCsvFile').mockImplementation(() => Promise.resolve());;
            const persistAnswersMethod = jest.spyOn(examService, 'persistAnswers').mockImplementation(() => Promise.resolve());
            
            const candidateAnswers: CreateUserAnswerDto[] = [];
            const readCsvContent = jest.spyOn(csvHelper, 'readCsvContent').mockImplementation(() => candidateAnswers);
            
            const file = '';
            await examService.persistCandidateAnswers(file).then(() => {
                expect(validateCsvCall).toHaveBeenCalledTimes(1);
                expect(readCsvContent).toHaveBeenCalledTimes(1);
                expect(persistAnswersMethod).toHaveBeenCalledTimes(1);
                done();
            });
        });
    });

    describe('Process Candidate Result', () => {
        
        test('Should throw Exception when candidate has not answered the exam', async (done) => {
            jest.spyOn(examRepository, 'checkIfExamExists').mockImplementation(() => Promise.resolve(false));
            await examService.processCandidateResult(search).catch(err => {
                expect(err).toBeInstanceOf(BusinessException);
                done();
            })
        });

        test('Should calculate Scores when candidate has answered the exam', async (done) => {

            const examTemplate = getExamTemplateSample();
            const candidateAttempt = getCandidateAttemptSample(search);
            const allCandidateAttempts = [candidateAttempt];

            //Arrange
            jest.spyOn(examService, 'validateCandidateAttemptToExam').mockImplementation(() => Promise.resolve());
            const findExamTemplate = jest.spyOn(examRepository, 'findExamTemplateWithCorrectAnswers').mockImplementation(() => Promise.resolve(examTemplate));
            const findCandidateAttempt = jest.spyOn(userAnswerService, 'findCandidateAnswersAtExam').mockImplementation(() => Promise.resolve(candidateAttempt));
            const findAllCandidateAttempts = jest.spyOn(userAnswerService, 'findAllCandidateAttemptToExam').mockImplementation(() => Promise.resolve(allCandidateAttempts));
           
            //Act
            const csvContent = await examService.processCandidateResult(search);
   
            //Assert
            expect(csvContent).toBeDefined();
            expect(csvContent.candidateEmail).toEqual(search.candidateEmail);
            expect(csvContent.candidateName).toBeDefined();
            expect(csvContent.avgScore).toMatch(new RegExp('\\d*\.\\d{2}$'));
            expect(csvContent.score).toMatch(new RegExp('\\d*\.\\d{2}$'));
            
            expect(findExamTemplate).toHaveBeenCalledTimes(1);
            expect(findCandidateAttempt).toHaveBeenCalledTimes(1);
            expect(findAllCandidateAttempts).toHaveBeenCalledTimes(1);
            done();
        });
    });
});

function getCandidateAttemptSample(search: SearchCandidateExamInfoDto) {
    const candidateAttempt = new CandidateAttemptDto();
    const candidateAnsweredQuestion = new CandidateAnsweredQuestionDto(1, AnswerOptionEnum.A);
    candidateAttempt.name = "Candidate Name";
    candidateAttempt.email = search.candidateEmail;
    candidateAttempt.answers = [candidateAnsweredQuestion];
    return candidateAttempt;
}

function getExamTemplateSample() {

    const examTemplateQuestion1 = new QuestionDto();
    examTemplateQuestion1.question = 1;
    examTemplateQuestion1.rightAnswer = AnswerOptionEnum.A;

    const examTemplate = new ExamInformationDto();
    examTemplate.name = "any";
    examTemplate.questions = [examTemplateQuestion1];
    return examTemplate;
}
