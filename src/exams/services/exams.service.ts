import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { CreateUserAnswerDto } from 'src/exams/dto/candidate-answer.dto';
import { CandidateAttemptDto } from '../dto/candidate-attempt.dto';
import { CsvExportContent } from '../dto/csv/csv-result-content';
import { ExamInformationDto } from '../dto/exam-template.dto';
import { SearchCandidateExamInfoDto } from '../dto/search-candidate-info.dto';
import { BusinessException } from '../exceptions/business.exception';
import { CsvHelper } from '../helpers/csv-helper';
import { ExamResultEvaluationHelper } from '../helpers/exam-result-evaluation-helper';
import { ExamRepository } from '../repositories/exam.repository';
import { ExamEvaluationService } from './exam-evaluation.service';
import { UserAnswerService } from './user-answers.service';


@Injectable()
export class ExamsService {
  constructor(
    @InjectRepository(ExamRepository) private examRepository,
    private userAnswerService: UserAnswerService,
    private examEvaluationService: ExamEvaluationService,
    private csvHelper: CsvHelper,
  ) {}

  async validateCandidateAttemptToExam(searchContent: SearchCandidateExamInfoDto){
    const examId = searchContent.examId;
    const candidateEmail = searchContent.candidateEmail;
    await this.examRepository.checkIfExamExists(examId).then(isValid => {
      if (!isValid) 
        throw new BusinessException('Exam information cannot be found: ' + examId);
    }) 

    await this.userAnswerService.findCandidateAnswersAtExam(candidateEmail, examId)
    .then(candidateAttempt => {
      if (!(Array.isArray(candidateAttempt.answers) && candidateAttempt.answers.length > 0)) 
        throw new BusinessException('Candidate has not answered the given Exam: ' + examId);
    });
  }
  
  async  persistCandidateAnswers(file: any) {
    await this.csvHelper.validateCsvFile(file)

    const answersFromFile: CreateUserAnswerDto[] = this.csvHelper.readCsvContent(file);
    await this.persistAnswers(answersFromFile);
  }

  async persistAnswers(candidateAnswers: CreateUserAnswerDto[]) {
    const examsWithQuestions = await this.examRepository.getAllExams();

    const existingExams = new Map();
    examsWithQuestions.forEach((examInfo) => {
      existingExams.set(examInfo.name, examInfo);
    });

    candidateAnswers.forEach((candidateAttempt) => {
      const relatedExam = existingExams.get(candidateAttempt.examName);
      const relatedQuestion = relatedExam.questions.filter(
        (x) => x.question == candidateAttempt.questionNumber,
      )[0];

      candidateAttempt.questionId = relatedQuestion.id;
      this.userAnswerService.create(candidateAttempt);
    });
  }

  async processCandidateResult(searchDetail: SearchCandidateExamInfoDto): Promise<CsvExportContent> {
    await this.validateCandidateAttemptToExam(searchDetail);
    
    const examName = searchDetail.examId;
    const candidateEmail = searchDetail.candidateEmail;

    const examTemplate: ExamInformationDto = await this.examRepository.findExamTemplateWithCorrectAnswers(examName);
    
    const examAnswersFromCandidate: CandidateAttemptDto = await this.userAnswerService.findCandidateAnswersAtExam(candidateEmail, examName);
    const candidateScore = this.examEvaluationService.calculateScore(examTemplate, examAnswersFromCandidate.answers);

    const examAnswersFromAllCandidates: CandidateAttemptDto[] = await this.userAnswerService.findAllCandidateAttemptToExam(examName);
    const allCandidatesScore: number[] = this.examEvaluationService.scoresInTheExam(examTemplate, examAnswersFromAllCandidates);

    const avgScore = ExamResultEvaluationHelper.avgScore(allCandidatesScore);
    const percentileRank = ExamResultEvaluationHelper.canculatePercentRank(allCandidatesScore, candidateScore);

    return new CsvExportContent(
      examName, 
      avgScore.toFixed(2), 
      examAnswersFromCandidate, 
      candidateScore.toFixed(2), 
      percentileRank.toFixed(2)
    );
  };
  
  
}
