import { Injectable } from '@nestjs/common';
import {
  CandidateAnsweredQuestionDto,
  CandidateAttemptDto
} from '../dto/candidate-attempt.dto';
import { ExamInformationDto } from '../dto/exam-template.dto';

@Injectable()
export class ExamEvaluationService {
  
  private percentage(rightAnswers: number, totalQuestions: number): number {
    const percent = ((rightAnswers / totalQuestions) * 100);
    return percent;
  }

  scoresInTheExam(examTemplate: ExamInformationDto, examAnswersFromAllCandidates: CandidateAttemptDto[]): number[] {
    const candidateScores: number[] = [];
    examAnswersFromAllCandidates.forEach((candidateAttempt) => {
      const score: number  = this.calculateScore(examTemplate, candidateAttempt.answers)
      candidateScores.push(score);
    });
    return candidateScores;
  }

  calculateScore(examTemplate: ExamInformationDto, examAnswersFromCandidate: CandidateAnsweredQuestionDto[]): number {
    const totalQuestions = examTemplate.questions.length;
    let rightAnswers = 0;
    rightAnswers = this.calculateRightAnswers(examTemplate, examAnswersFromCandidate, rightAnswers);
      
    const result: number = this.percentage(rightAnswers, totalQuestions);
    return result;
  }

  private calculateRightAnswers(examTemplate: ExamInformationDto, examAnswersFromCandidate: CandidateAnsweredQuestionDto[], rightAnswers: number) {
    examTemplate.questions.forEach((examQuestion) => {
      const answeredQuestion = examAnswersFromCandidate.filter((x) => x.questionNumber == examQuestion.question)[0];
      if (answeredQuestion != undefined && answeredQuestion.answer == examQuestion.rightAnswer) {
        rightAnswers++;
      }
    });
    return rightAnswers;
  }
}
