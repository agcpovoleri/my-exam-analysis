import * as _ from 'lodash';

export class ExamResultEvaluationHelper {
  
  static avgScore(scores: number[]): number {
    const sum: number = scores.reduce((a, b) => a + b, 0);
    const avg: number = sum / scores.length || 0;
    return avg;
  }

  static canculatePercentRank(scores: number[], threshold: number) {
    if (this.isEmptyScoreList(scores)) return NaN;
    
    if (this.isExistingScore(scores, threshold)) {
      return (this.percentileRank(scores, threshold));
    }
    return (this.interpolationOfScores(scores, threshold));
  }

  private static percentileRank(scores: number[], threshold: number) {
    
    const lowerScores = scores.filter((currentScore) => currentScore < threshold).length;
    const higherScores = scores.filter((currentScore) => currentScore > threshold).length;
    
    if (lowerScores === 0 && higherScores === 0 && this.isUniqueScore(scores)){
      return 1;
    }
    const result = this.getPercentRankResult(lowerScores, higherScores);
    return result;
  }

  private static isEmptyScoreList(scores: number[]): boolean {
    return scores.length === 0;
  }

  private static isExistingScore(scores: number[], threshold: number): boolean {
    const existingValue = scores.find(v => v === threshold);
    return existingValue != undefined;
  }

  private static isUniqueScore(scores: number[]) {
    const differentScores = Array.from(new Set(scores.map((item: any) => item)));
    return differentScores.length === 1;
  }

  static getPercentRankResult(lowerScores:number, higherScores:number) {
    return lowerScores / (lowerScores + higherScores);
  }

  static getInterpolationResult(resultOfBelowValues:number, resultOfAboveValues:number) : number{
    const interpolationPercent = 0.68;
    return resultOfBelowValues + (interpolationPercent * (resultOfAboveValues - resultOfBelowValues));
  }

  static interpolationOfScores(scores: number[], score: number) {
    
    const biggerValues = scores.filter(v => v > score);
    const lowerValues = scores.filter(v => v < score);
    
    if (biggerValues.length === 0 || lowerValues.length === 0) return NaN; 
    
    const resultOfAboveValues = this.percentileRank(scores, _.min(scores.filter(v => v > score)));
    const resultOfBelowValues = this.percentileRank(scores, _.max(scores.filter(v => v < score)));

    return this.getInterpolationResult(resultOfBelowValues, resultOfAboveValues);
  }
}
