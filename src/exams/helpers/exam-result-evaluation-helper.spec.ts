import { ExamResultEvaluationHelper } from './exam-result-evaluation-helper';

describe('ExamResultEvaluationHelper', () => {
  describe('average score', () => {
    test('Expect avg of empty array to be 0', () => {
      const expectedAvg = 0;
      const scores = [];
      const avg = ExamResultEvaluationHelper.avgScore(scores);
      expect(avg).toBe(expectedAvg);
    });

    test('Expect avg of single element (0) at array to be equals the element', () => {
      const expectedAvg = 0;
      const scores = [0];
      const avg = ExamResultEvaluationHelper.avgScore(scores);
      expect(avg).toBe(expectedAvg);
    });

    test('Expect avg of single element (any) at array to be equals the element', () => {
      const expectedAvg = 1;
      const scores = [1];
      const avg = ExamResultEvaluationHelper.avgScore(scores);
      expect(avg).toBe(expectedAvg);
    });

    test('Expect avg of equal values to be the element', () => {
      const expectedAvg = 1;
      const scores = [1, 1];
      const avg = ExamResultEvaluationHelper.avgScore(scores);
      expect(avg).toBe(expectedAvg);
    });

    test('Expect avg of different values to the sum div 2', () => {
      const expectedAvg = 3;
      const scores = [2, 4];
      const avg = ExamResultEvaluationHelper.avgScore(scores);
      expect(avg).toBe(expectedAvg);
    });

    test('Expect avg of some elements to have decimal cases', () => {
      const expectedAvg = 1.5;
      const scores = [1, 2];
      const avg = ExamResultEvaluationHelper.avgScore(scores);
      expect(avg).toBe(expectedAvg);
    });
  });

  describe('calculatePercentRank', () => {
    
    test('Expect valid score when element is in the middle of the list of scores', () => {
        const scores = [8,5,4,3,2];
        const score = 4;
        const percentRankScore = ExamResultEvaluationHelper.canculatePercentRank(scores, score);

        expect(percentRankScore).toBeLessThan(1);
        expect(percentRankScore).toBeGreaterThan(0);
        expect(percentRankScore).toBe(0.5);
    })
    test('Expect valid score when element is in the middle of the list of scores', () => {
        const expectedPercentRank = 0.92;
        const scores = [8,5,4,3,2];
        const score = 7;

        const percentRankScore = ExamResultEvaluationHelper.canculatePercentRank(scores, score);

        expect(percentRankScore).toBeLessThan(1);
        expect(percentRankScore).toBeGreaterThan(0);
        expect(percentRankScore).toBe(expectedPercentRank);
    })
    test('Expect valid score when element is one in the middle of the list of scores', () => {
      const expectedPercentRank = 0.25;
      const scores = [8,5,4,3,2];
      const score = 3;

      const percentRankScore = ExamResultEvaluationHelper.canculatePercentRank(scores, score);

      expect(percentRankScore).toBeLessThan(1);
      expect(percentRankScore).toBeGreaterThan(0);
      expect(percentRankScore).toBe(expectedPercentRank);
  })
  test('Expect valid score when multiple elements is one in the middle of the list of scores', () => {
    const scores = [8,5,4,3,3,3,3,2];
    const score = 3;

    const percentRankScore = ExamResultEvaluationHelper.canculatePercentRank(scores, score);

    expect(percentRankScore).toBeLessThan(1);
    expect(percentRankScore).toBeGreaterThan(0);
  })
    test('Expect the highest score when given element is higher than the max on the list of scores', () => {
        const scores = [8,5,4,3,2];
        const score = 9;

        const percentRankScore = ExamResultEvaluationHelper.canculatePercentRank(scores, score);

        expect(percentRankScore).toBeNaN();
    })
    test('Expect the highest score when given element is lower than the max on the list of scores', () => {
      const scores = [8,5,4,3,2];
      const score = 1;

      const percentRankScore = ExamResultEvaluationHelper.canculatePercentRank(scores, score);

      expect(percentRankScore).toBeNaN();
  })
    test('Expect Not a Number score when empty list', () => {
      
      const scores = [];
      const score = 80;

      const percentRankScore = ExamResultEvaluationHelper.canculatePercentRank(scores, score);

      expect(percentRankScore).toBeNaN();
    })
    test('Expect highest score when single element on the list correspond the current score', () => {
      const scores = [80];
      const score = 80;

      const percentRankScore = ExamResultEvaluationHelper.canculatePercentRank(scores, score);

      expect(percentRankScore).toBe(1);
    })

    test('Expect highest score when multiple elements on the list correspond the current score', () => {
      const scores = [80,80,80,80];
      const score = 80;

      const percentRankScore = ExamResultEvaluationHelper.canculatePercentRank(scores, score);

      expect(percentRankScore).toBe(1);
    })

    test('Expect lowest score when element is the lowest on the list of scores', () => {
      const expectedPercentRank = 0;
      const scores = [8,5,4,3,2];
      const score = 2;

      const percentRankScore = ExamResultEvaluationHelper.canculatePercentRank(scores, score);

      expect(percentRankScore).toBe(expectedPercentRank);
    })
  })
});
