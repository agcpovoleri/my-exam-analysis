import { Test } from '@nestjs/testing';
import { CsvModule, CsvParser } from 'nest-csv-parser';
import { Readable } from "stream";
import { CreateUserAnswerDto } from '../dto/candidate-answer.dto';
import { CandidateAttemptDto } from '../dto/candidate-attempt.dto';
import { CsvExportContent } from '../dto/csv/csv-result-content';
import { InvalidCSVContentException } from '../exceptions/invalidcontent.exception';
import { CsvHelper } from './csv-helper';

    let csvHelper: CsvHelper;
    let csvParser: CsvParser;

    const readableStream = new Readable();
    readableStream._read = function() { /* do nothing */ };

    beforeEach(async (done) => {
        const moduleRef = await Test.createTestingModule({
            controllers: [],
            providers: [CsvHelper],
            imports: [CsvModule]
        }).compile();

        csvHelper = moduleRef.get<CsvHelper>(CsvHelper);
        csvParser = moduleRef.get<CsvParser>(CsvParser);
        done();
    });


describe('CsvHelper', () => {
  
  describe('Read CSV content', () => {
    test('Should throw exception if content of the file is invalid', () => {
      jest.spyOn(csvHelper, 'parseCsvContentIntoCandidateAnswers').mockImplementation(() => {
        throw new Error();
      });
      try{
        csvHelper.readCsvContent('any');
      } catch (err) {
        expect(err).toBeInstanceOf(InvalidCSVContentException);
      }
    });

    test('Should parse content from CSV file into CandidateAnswers array', () => {
      //Arrange
      const expectedCandidateAnswers: CreateUserAnswerDto[] = [];
      jest.spyOn(csvHelper, 'parseCsvContentIntoCandidateAnswers').mockImplementation(() => expectedCandidateAnswers);
      //Act
      const candidateAnswers = csvHelper.readCsvContent('any');
      //Assert
      expect(candidateAnswers).toBeInstanceOf(Array);
    });
  })

  describe('Validate CSV content', () => {
    
    test('Should throw exception if content validation is invalid', async (done) => {
      
      jest.spyOn(csvHelper, 'convertFileBufferToStream').mockImplementation(() => {return readableStream});
      jest.spyOn(csvParser, 'parse').mockImplementation(() => {
        throw new Error();
      });
      
      await csvHelper.validateCsvFile('any').catch((err) => {
        expect(err).toBeInstanceOf(InvalidCSVContentException);
        done();
      });
    });

    test('Should validation works as expected if expected data is parsed from the CSV file', async (done) => {
      
      jest.spyOn(csvHelper, 'convertFileBufferToStream').mockImplementation(() => {return readableStream});
      jest.spyOn(csvParser, 'parse').mockImplementation(() => Promise.resolve(null));
      
      await csvHelper.validateCsvFile('any').then(() => {
        done();
      });
      
    });
  });

  describe('Download candidate attempt result ', () => {
    
    const candidateAttempt = new CandidateAttemptDto();
    candidateAttempt.name = "CandidateName";
    candidateAttempt.email = "CandidateEmail";

    const examName = 'EXAM1111';
    const candidateScore =  '50.00';
    const examAvgScore =  '75.00';
    const examPercentRank = '0.50';
    const candidateExamResult = new CsvExportContent(examName, candidateScore, candidateAttempt, examAvgScore, examPercentRank);

    test('Should CSV content contains values retrieved at Candidate Exam Result', () => {
      const csvContent = csvHelper.downloadResource(candidateExamResult);
      
      expect(csvContent).toContain(examName);
      expect(csvContent).toContain(candidateAttempt.name);
      expect(csvContent).toContain(candidateAttempt.email);
      expect(csvContent).toContain(candidateScore);
      expect(csvContent).toContain(examAvgScore);
      expect(csvContent).toContain(examPercentRank);
    });
  });

    
});
