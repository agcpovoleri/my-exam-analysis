import { Injectable, Logger } from '@nestjs/common';
import * as csv from 'csv-parser';
import { Parser } from 'json2csv';
import { CsvParser } from 'nest-csv-parser';
import { CreateUserAnswerDto } from '../../../src/exams/dto/candidate-answer.dto';
import { Entity } from 'typeorm';
import { Readable } from 'typeorm/platform/PlatformTools';
import { CsvExportContent, CsvExportFields } from '../dto/csv/csv-result-content';
import { InvalidCSVContentException } from '../exceptions/invalidcontent.exception';

@Injectable()
export class CsvHelper {
  
  FILE_SEPARATOR  = '|';

  constructor(private readonly csvParser: CsvParser) {}
  
  readCsvContent(file: any): CreateUserAnswerDto[] {
    let results: CreateUserAnswerDto[];
    try {
      results = this.parseCsvContentIntoCandidateAnswers(file);
    } catch (e) {
      Logger.error('Error while reading csv file');
      throw new InvalidCSVContentException();
    } 
    return results;
  }

  parseCsvContentIntoCandidateAnswers(file: any): CreateUserAnswerDto[] {
    const candidateAnswers: CreateUserAnswerDto[] = [];
    this.convertFileBufferToStream(file.buffer)
      .pipe(csv({ separator: this.FILE_SEPARATOR }))
      .on('data', (data) => {
        const candidateAnswer = new CreateUserAnswerDto(data);
        candidateAnswers.push(candidateAnswer);
      })
      .on('end', () => {
        Logger.log('Valid CSV file was properly parsed containing ' + candidateAnswers.length + ' lines');
      });
    return candidateAnswers;
  }

  async validateCsvFile(file: any) {
    try {
      const stream = this.convertFileBufferToStream(file.buffer);
      await this.csvParser.parse(stream, Entity, null, null, {
        strict: true,
        separator: this.FILE_SEPARATOR,
      });
    } catch (e) {
      Logger.error('Error parsing CSV file');
      throw new InvalidCSVContentException();
    } 
  }
  
  downloadResource(data: CsvExportContent): any {
    
    const json2csv = new Parser({ fields: CsvExportFields, delimiter: this.FILE_SEPARATOR, quote : ''});
    const csv = json2csv.parse(data);
    return csv;
  };
  
  convertFileBufferToStream(binary) {
    return new Readable({
      read() {
          this.push(binary);
          this.push(null);
      }
    });
  }

}
