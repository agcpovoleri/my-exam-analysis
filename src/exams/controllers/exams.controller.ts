import {
  Controller, Get, Param, Post, Res, UploadedFile, UseInterceptors
} from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import {
  ApiBadRequestResponse,
  ApiConsumes,
  ApiCreatedResponse,
  ApiNotAcceptableResponse,
  ApiNotFoundResponse,
  ApiOkResponse,
  ApiOperation,
  ApiParam,
  ApiProduces
} from '@nestjs/swagger';
import { ApiFileUpload } from '../../../src/common/decorators/file-upload.decorator';
import { ImageFileFilter } from '../../../src/common/filters/filetype.filter';
import { SearchCandidateExamInfoDto } from '../dto/search-candidate-info.dto';
import { CsvHelper } from '../helpers/csv-helper';
import { ExamsService } from '../services/exams.service';

@Controller('v1/exams')
export class ExamsController {
  constructor(
    private readonly examsService: ExamsService,
    private readonly csvHelper: CsvHelper,
  ) { }

  @ApiOperation({ summary: 'Retrieve exam result of given candidate' })
  @ApiOkResponse({ description: 'Exam results sucessfully processed' })
  @ApiBadRequestResponse({ description: 'The mandatory parameters were not informed .' })
  @ApiNotFoundResponse({ description: 'Exam/candidate not found for the provided values' })
  @ApiProduces('text/csv')
  @ApiParam({ name: 'examId', required: true, example: 'EX202001', description: 'Name/Identifier or the exam' })
  @ApiParam({ name: 'candidateEmail', required: true, example: 'email@domain.com', description: 'Candidate email that performed the exam attempt' })
  @Get(':examId/candidates/:candidateEmail')
  async findCandidateScore(@Param() searchDetail: SearchCandidateExamInfoDto, @Res() res) {
    
    await this.examsService.processCandidateResult(searchDetail).then((data) => {
      const csvContent = this.csvHelper.downloadResource(data);
      res.header('Content-Type', 'text/csv');
      res.attachment('candidateScore.csv');
      return res.send(csvContent);
    });
  }
  
  @ApiOperation({ summary: 'Persist candidate answers based on the CSV input file' })
  @ApiCreatedResponse({description: 'The exams have been successfully created.'})
  @ApiBadRequestResponse({description: 'Request was not properly processed due to invalid content'})
  @ApiNotAcceptableResponse({description:'Request could not been processed due to invalid media type. Only CSV is acceptable'})
  @ApiConsumes('multipart/form-data')
  @ApiFileUpload('file', 'CSV file containing candidate answers', true)
  @UseInterceptors(FileInterceptor('file', { 
    fileFilter: ImageFileFilter 
  }))
  @Post('candidate-answers')
  async persistCandidateAnswers(@UploadedFile() file) {
    await this.examsService.persistCandidateAnswers(file);
  }
}
