import { Test } from '@nestjs/testing';
import { CsvModule, CsvParser } from 'nest-csv-parser';
import { CandidateAttemptDto } from '../dto/candidate-attempt.dto';
import { CsvExportContent } from '../dto/csv/csv-result-content';
import { SearchCandidateExamInfoDto } from '../dto/search-candidate-info.dto';
import { BusinessException } from '../exceptions/business.exception';
import { CsvHelper } from '../helpers/csv-helper';
import { CandidateAnswerRepository } from '../repositories/candidate-answer.repository';
import { ExamRepository } from '../repositories/exam.repository';
import { ExamEvaluationService } from '../services/exam-evaluation.service';
import { ExamsService } from '../services/exams.service';
import { UserAnswerService } from '../services/user-answers.service';
import { ExamsController } from './exams.controller';

describe('ExamsController', () => {

    let examsController: ExamsController;
    let examsService: ExamsService;
    let csvHelper: CsvHelper;

    const search = new SearchCandidateExamInfoDto();
    search.examId = "any";
    search.candidateEmail = "any"

    beforeEach(async (done) => {
        const moduleRef = await Test.createTestingModule({
            controllers: [],
            providers: [ExamsController,
                ExamsService,
                ExamRepository,
                ExamEvaluationService,
                UserAnswerService,
                CandidateAnswerRepository,
                CsvHelper,
                CsvParser],
            imports: [CsvModule]
        }).compile();

        examsController = moduleRef.get<ExamsController>(ExamsController);
        examsService = moduleRef.get<ExamsService>(ExamsService);
        csvHelper = moduleRef.get<CsvHelper>(CsvHelper);
        done();
    });

    describe('Uploading Candidate Answers', () => {
        test('Should process the candidate answers if validation does not throw exception', async (done) => {
            const persistCandidateAnswersMethod = jest.spyOn(examsService, 'persistCandidateAnswers').mockImplementation(() => Promise.resolve());
            
            const file = 'any';
            await examsController.persistCandidateAnswers(file).then(() => {
                expect(persistCandidateAnswersMethod).toHaveBeenCalledTimes(1);
                done();
            });
        })
    });

    describe('Finding Candidate Score', () => {

        const candidateExamResult = getCandidateResultAtExam();
        const response = {
            content_type_header: null,
            content_type_value: null,
            attachment_name: null,
            attachment_content: null,

            header: function(key: string, value: string) { 
                this.content_type_header = key;
                this.content_type_value = value;
            },
            attachment: function(name: string) {
                this.attachment_name = name;
            },
            send: function(content: string) {
                this.attachment_content = content;
            }
        };

        test('Should throw exception if exam/candidate answer was not found', async (done) => {
            jest.spyOn(examsService, 'validateCandidateAttemptToExam').mockImplementation(() => {
                throw new BusinessException('Err');
            });
            await examsController.findCandidateScore(search, response).catch(err => {
                expect(err).toBeInstanceOf(BusinessException);
                done();
            })
        });

        test('Should process the candidate answers if validation does not throw exception', async (done) => {
            const csvContentToDownload = jest.spyOn(examsService, 'processCandidateResult').mockImplementation(() => Promise.resolve(candidateExamResult));
            const csvParse = jest.spyOn(csvHelper, 'downloadResource');
            
            await examsController.findCandidateScore(search, response).then(() => {
                expect(csvContentToDownload).toHaveBeenCalledTimes(1);
                expect(csvParse).toHaveBeenCalled();

                expect(response.content_type_header).toBe('Content-Type');
                expect(response.content_type_value).toBe('text/csv');
                expect(response.attachment_content).toBeDefined();
                done();
            });
        })
    });
});

function getCandidateResultAtExam() {
    const candidateAttempt = new CandidateAttemptDto();
    candidateAttempt.name = "CandidateName";
    candidateAttempt.email = "CandidateEmail";

    const examName = 'EXAM1111';
    const candidateScore = '50.00';
    const examAvgScore = '75.00';
    const examPercentRank = '0.50';
    const candidateExamResult = new CsvExportContent(examName, candidateScore, candidateAttempt, examAvgScore, examPercentRank);
    return candidateExamResult;
}
