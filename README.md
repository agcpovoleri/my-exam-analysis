## Description

Example application in Typescript / NestJS that validate exam results and how candidate performed based on other candidates.
The analysis include the score of the candidate on the given Exam; the average score of other candidates on the same exam; the percentrank calc based on https://support.microsoft.com/en-us/office/percentrank-exc-function-d8afee96-b7e2-4a2f-8c01-8fcdedaa6314.

## Requirements

1. - Postgres database instance running based on configuration available at 'ormconfig.json' file
1. - Create an Database with name: 'my_exam_db'
1. - Run scripts to create table (etc/db_scripts/init_db_script.sql) and if needs drop all tables (etc/db_scripts/clean_db.sql)

## Useful files

Useful files to help on testing can be found at (etc/csv_files)

## Installation

```bash
$ npm install
```

## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```
## Accessing the application

The built application contains an swagger API interface that can be accessed by 'http://localhost:3000/api/'

The Swagger API contains 2 endpoints that is part of the POC/business logic:

* GET /v1/exams/{examId}/candidates/{candidateEmail}

* POST /v1/exams/candidate-answers

## Test

```bash
# unit tests
$ npm run test

# test coverage
$ npm run test:cov
```

## Upcoming Improvements

There are few improvements that are coming to this code.

- [ ] Improvement of JEST tests: E2E, Integration, Unit tests
- [ ] Environment creation with Docker (Postgres + NodeJs) including TypeORM Migration scripts for db preparation
- [ ] Normalization of Database entities to avoid data repetition at User_Answers table
- [ ] Usage of UUID as primary key for all Entities (Entity 
- [ ] Improve usage of Async/Await methods


## Support

Nest is an MIT-licensed open source project. It can grow thanks to the sponsors and support by the amazing backers. If you'd like to join them, please [read more here](https://docs.nestjs.com/support).

## Stay in touch

- Author - [Augusto Povoleri](https://kamilmysliwiec.com)

## License

Nest is [MIT licensed](LICENSE).
