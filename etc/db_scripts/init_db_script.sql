

CREATE TABLE exams (
	id SERIAL PRIMARY KEY,
	name VARCHAR (8) not null UNIQUE);

INSERT INTO exams (name) VALUES ('EX202001');
INSERT INTO exams (name) VALUES ('EX202002');
INSERT INTO exams (name) VALUES ('EX202003');
INSERT INTO exams (name) VALUES ('EX202004');


DROP TABLE questions;
CREATE TABLE questions (
	id SERIAL PRIMARY KEY,
	exam_id int NOT NULL,
	question int NOT NULL,
	answer VARCHAR (1),
	UNIQUE(exam_id, question),
	CONSTRAINT fk_examp_questions
      FOREIGN KEY(exam_id) 
	  REFERENCES exams(id)
);
--EX202001 (10 questions) : 1-A, 2-C, 3-C, 4-A, 5-B, 6-B, 7-C, 8-A, 9-D, 10-A
INSERT INTO QUESTIONS (exam_id, question, answer) VALUES (1,1,'A');
INSERT INTO QUESTIONS (exam_id, question, answer) VALUES (1,2,'C');
INSERT INTO QUESTIONS (exam_id, question, answer) VALUES (1,3,'C');
INSERT INTO QUESTIONS (exam_id, question, answer) VALUES (1,4,'A');
INSERT INTO QUESTIONS (exam_id, question, answer) VALUES (1,5,'B');
INSERT INTO QUESTIONS (exam_id, question, answer) VALUES (1,6,'B');
INSERT INTO QUESTIONS (exam_id, question, answer) VALUES (1,7,'C');
INSERT INTO QUESTIONS (exam_id, question, answer) VALUES (1,8,'A');
INSERT INTO QUESTIONS (exam_id, question, answer) VALUES (1,9,'D');
INSERT INTO QUESTIONS (exam_id, question, answer) VALUES (1,10,'A');
--EX202002 (17 questions): 1-B, 2-D, 3-C, 4-C, 5-A, 6-D, 7-E, 8-E, 9-C, 10-A, 11:E, 12:C, 13:D, 14:E, 15:A, 16:B, 17:D
INSERT INTO QUESTIONS (exam_id, question, answer) VALUES (2,1,'B');
INSERT INTO QUESTIONS (exam_id, question, answer) VALUES (2,2,'D');
INSERT INTO QUESTIONS (exam_id, question, answer) VALUES (2,3,'C');
INSERT INTO QUESTIONS (exam_id, question, answer) VALUES (2,4,'C');
INSERT INTO QUESTIONS (exam_id, question, answer) VALUES (2,5,'A');
INSERT INTO QUESTIONS (exam_id, question, answer) VALUES (2,6,'D');
INSERT INTO QUESTIONS (exam_id, question, answer) VALUES (2,7,'E');
INSERT INTO QUESTIONS (exam_id, question, answer) VALUES (2,8,'E');
INSERT INTO QUESTIONS (exam_id, question, answer) VALUES (2,9,'C');
INSERT INTO QUESTIONS (exam_id, question, answer) VALUES (2,10,'A');
INSERT INTO QUESTIONS (exam_id, question, answer) VALUES (2,11,'E');
INSERT INTO QUESTIONS (exam_id, question, answer) VALUES (2,12,'C');
INSERT INTO QUESTIONS (exam_id, question, answer) VALUES (2,13,'D');
INSERT INTO QUESTIONS (exam_id, question, answer) VALUES (2,14,'E');
INSERT INTO QUESTIONS (exam_id, question, answer) VALUES (2,15,'A');
INSERT INTO QUESTIONS (exam_id, question, answer) VALUES (2,16,'B');
INSERT INTO QUESTIONS (exam_id, question, answer) VALUES (2,17,'D');
--EX202003 (20 questions): 
--1-B, 2-C, 3-E, 4-E, 5-A, 6-C, 7-D, 8-E, 9-A, 10-A, 11:E, 12:C, 13:B, 14:A, 15:B, 16:C, 17:A, 18:B, 19:B, 20:D
INSERT INTO QUESTIONS (exam_id, question, answer) VALUES (3,1,'B');
INSERT INTO QUESTIONS (exam_id, question, answer) VALUES (3,2,'C');
INSERT INTO QUESTIONS (exam_id, question, answer) VALUES (3,3,'E');
INSERT INTO QUESTIONS (exam_id, question, answer) VALUES (3,4,'E');
INSERT INTO QUESTIONS (exam_id, question, answer) VALUES (3,5,'A');
INSERT INTO QUESTIONS (exam_id, question, answer) VALUES (3,6,'C');
INSERT INTO QUESTIONS (exam_id, question, answer) VALUES (3,7,'D');
INSERT INTO QUESTIONS (exam_id, question, answer) VALUES (3,8,'E');
INSERT INTO QUESTIONS (exam_id, question, answer) VALUES (3,9,'A');
INSERT INTO QUESTIONS (exam_id, question, answer) VALUES (3,10,'A');
INSERT INTO QUESTIONS (exam_id, question, answer) VALUES (3,11,'E');
INSERT INTO QUESTIONS (exam_id, question, answer) VALUES (3,12,'C');
INSERT INTO QUESTIONS (exam_id, question, answer) VALUES (3,13,'B');
INSERT INTO QUESTIONS (exam_id, question, answer) VALUES (3,14,'A');
INSERT INTO QUESTIONS (exam_id, question, answer) VALUES (3,15,'B');
INSERT INTO QUESTIONS (exam_id, question, answer) VALUES (3,16,'C');
INSERT INTO QUESTIONS (exam_id, question, answer) VALUES (3,17,'A');
INSERT INTO QUESTIONS (exam_id, question, answer) VALUES (3,18,'B');
INSERT INTO QUESTIONS (exam_id, question, answer) VALUES (3,19,'B');
INSERT INTO QUESTIONS (exam_id, question, answer) VALUES (3,20,'D');
--EX202004 (12 questions): 1-E, 2-A, 3-C, 4-D, 5-E, 6-A, 7-A, 8:E, 9:C, 10:B, 11:A, 12:B
INSERT INTO QUESTIONS (exam_id, question, answer) VALUES (4,1,'E');
INSERT INTO QUESTIONS (exam_id, question, answer) VALUES (4,2,'A');
INSERT INTO QUESTIONS (exam_id, question, answer) VALUES (4,3,'C');
INSERT INTO QUESTIONS (exam_id, question, answer) VALUES (4,4,'D');
INSERT INTO QUESTIONS (exam_id, question, answer) VALUES (4,5,'E');
INSERT INTO QUESTIONS (exam_id, question, answer) VALUES (4,6,'A');
INSERT INTO QUESTIONS (exam_id, question, answer) VALUES (4,7,'A');
INSERT INTO QUESTIONS (exam_id, question, answer) VALUES (4,8,'E');
INSERT INTO QUESTIONS (exam_id, question, answer) VALUES (4,9,'C');
INSERT INTO QUESTIONS (exam_id, question, answer) VALUES (4,10,'B');
INSERT INTO QUESTIONS (exam_id, question, answer) VALUES (4,11,'A');
INSERT INTO QUESTIONS (exam_id, question, answer) VALUES (4,12,'B');

CREATE TABLE user_answers (
	id SERIAL PRIMARY KEY,
	exam_date DATE not null,
	question_id BIGINT NOT NULL,
	answer VARCHAR (1),
	candidate_name VARCHAR(100) NOT NULL,
	candidate_email VARCHAR(100) NOT NULL,
	CONSTRAINT fk_questions
      FOREIGN KEY(question_id) 
	  REFERENCES questions(id)
);